package fr.shyndard.alteryaeconomy.event;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.meowj.langutils.lang.LanguageHelper;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaeconomy.Main;
import fr.shyndard.alteryaeconomy.function.GlobalFunction;
import fr.shyndard.alteryaeconomy.manager.ShopManager;
import net.md_5.bungee.api.ChatColor;

public class PlayerInteractShop implements Listener {

	private HashMap<Player, Long> interact_player = new HashMap<>();
	public static HashMap<Location, ArmorStand> armorstand_location = new HashMap<>();
	
	@EventHandler
	public void onPlayerInteract(BlockBreakEvent event) {
	    BlockState eventBlockState = event.getBlock().getState();
	    if ((eventBlockState instanceof Sign)) {
	    	Sign sign = (Sign)eventBlockState;
	    	if(sign.getLine(0).equals(Main.getFirstSignLine()) && event.getPlayer() != null && ShopManager.isShop(event.getBlock().getLocation())) {
	    		if(event.getPlayer().isSneaking()) {
		    		if(ShopManager.isAdminShop(event.getBlock().getLocation())) {
		    			if(Main.getEditorList().contains(event.getPlayer())) {
		    				removeShop(event.getBlock().getLocation(), event.getPlayer());
		    			} else event.setCancelled(true);
		    		} else removeShop(event.getBlock().getLocation(), event.getPlayer());
	    		} else {
	    			event.setCancelled(true);
	    		}
	    	}
	    }
	}
	
	private void removeShop(Location loc, Player player) {
		ShopManager.removeShop(loc);
		player.sendMessage(Main.getShopPrefix() + "Shop " + ChatColor.RED + "supprim�" + ChatColor.GRAY + ".");
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block eventBlock = event.getClickedBlock();
		    BlockState eventBlockState = eventBlock.getState();
		    if ((eventBlockState instanceof Sign)) {
		    	Sign sign = (Sign)eventBlockState;
		    	if(sign.getLine(0).equals(Main.getFirstSignLine()) && !Main.getEditorList().contains(event.getPlayer()) && !event.getPlayer().isSneaking()) {
		    		if(Main.add_admin_shop.contains(event.getPlayer())) {
		    			ShopManager.addAdminShop(event.getClickedBlock().getLocation());
		    			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.GREEN + "Ajout effectu�");
		    			return;
		    		}
		    		if(interact_player.get(event.getPlayer()) != null && interact_player.get(event.getPlayer())+1 > System.currentTimeMillis()/1000) {
		    			event.getPlayer().sendMessage(Main.getShopPrefix() + "N'allez pas trop vite.");
		    			return;	
		    		}
		    		interact_player.put(event.getPlayer(), System.currentTimeMillis()/1000);
		    		try {
			    		String[] item_data = sign.getLine(1).split(":");
			    		int amount = Integer.parseInt(sign.getLine(3));
			    		ItemStack is = null;
			    		if(item_data.length == 2) {
			    			is = new ItemStack(Material.matchMaterial(item_data[0]), amount, Short.parseShort(item_data[1]));
			    		} 
			    		else if(item_data.length == 1) {
			    			is = new ItemStack(Material.matchMaterial(item_data[0]), amount);
			    		}
			    		if(Main.getArmorStandList().contains(event.getPlayer())) {
			    			ArmorStand as = armorstand_location.get(eventBlock.getLocation());
			    			if(as == null) {
			    				BlockFace face = ((org.bukkit.material.Sign)eventBlock.getState().getData()).getFacing();
			    				Location armorLoc = eventBlock.getLocation().add(0,-0.6,0);
			    				if(is.getType().isBlock()) armorLoc = armorLoc.add(0,0.4,0);
			    				if(face.equals(BlockFace.EAST)) {
			    					armorLoc = armorLoc.add(-0.3,0,0.5);
			    				} else if(face.equals(BlockFace.WEST)) {
			    					armorLoc = armorLoc.add(+1.3,0,0.5);
			    				} else if(face.equals(BlockFace.NORTH)) {
			    					armorLoc = armorLoc.add(0.5,0,+1.3);
			    				} else if(face.equals(BlockFace.SOUTH)) {
			    					armorLoc = armorLoc.add(0.5,0,-0.3);
			    				}
			    				as = (ArmorStand)eventBlock.getWorld().spawnEntity(armorLoc, EntityType.ARMOR_STAND);
			    				as.setBasePlate(false);
				    			as.setCollidable(false);
				    			as.setVisible(false);
				    			as.setGravity(false);
				    			as.setHelmet(is);
				    			armorstand_location.put(eventBlock.getLocation(), as);
			    			} else {
			    				as.remove();
			    				armorstand_location.put(eventBlock.getLocation(), null);
			    			}
			    		} else if(ShopManager.isAdminShop(event.getClickedBlock().getLocation())) {
			    			if(DataAPI.getPlayer(event.getPlayer()).hasPermission("economy.useadminshop")) {
				    			String[] price_data = sign.getLine(2).split(" ");
					    		for(String price_action : price_data) {
					    			if(price_action.startsWith("A") && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					    				String[] price_action_data = price_action.split(":");
					    				Float price = Float.parseFloat(price_action_data[1]);
					    				if(DataAPI.getPlayer(event.getPlayer()).getMoney(false) >= price) {
					    					event.getPlayer().getInventory().addItem(is);
					    					DataAPI.getPlayer(event.getPlayer()).removeMoney(price, true, false);
					    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.GREEN + "Vous recevez " + amount + "x " + LanguageHelper.getItemName(is, "fr_FR") + ".");
					    				} else {
					    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous n'avez pas assez d'argent.");
					    				}
						    		} else if(price_action.startsWith("V") && event.getAction() == Action.LEFT_CLICK_BLOCK) {
						    			String[] price_action_data = price_action.split(":");
					    				Float price = Float.parseFloat(price_action_data[1]);
					    				int inventory_item_count = GlobalFunction.getItemCount(event.getPlayer().getInventory(), is);
					    				if(inventory_item_count < amount) {
					    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Minimum de "+amount+" pour vendre.");
					    					return;
					    				}
					    				GlobalFunction.removeItem(event.getPlayer(), is, amount);
					    				DataAPI.getPlayer(event.getPlayer()).addMoney(price, true, false);
					    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.GREEN + "Vous vendez "+amount+" " + LanguageHelper.getItemName(is, "fr_FR") + ".");
					    			}
				    			}
			    			} else {
			    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous n'avez pas la permission d'utiliser ce shop.");
			    			}
			    		} else if(ShopManager.isPlayerShop(event.getClickedBlock().getLocation())) {
			    			if(DataAPI.getPlayer(event.getPlayer()).hasPermission("economy.useplayershop")) {
					    		Block block = event.getClickedBlock().getWorld().getBlockAt(event.getClickedBlock().getLocation().clone().add(0,-1,0));
					    		if(block.getType() != Material.CHEST && block.getType() != Material.TRAPPED_CHEST) {
					    			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Coffre introuvable.");
					    		} else {
					    			String[] price_data = sign.getLine(2).split(" ");
					    			Chest chest = (Chest)block.getState();
					    			for(String price_action : price_data) {
					    				String[] price_action_data = price_action.split(":");
					    				Float price = Float.parseFloat(price_action_data[1]);
						    			if(price_action.startsWith("A") && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
						    				int chest_item_count = GlobalFunction.getItemCount(chest.getBlockInventory(), is);
						    				if(chest_item_count == 0) {
						    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Ce shop n'a plus d'item � vendre.");
						    				} else if(chest_item_count < amount) {
						    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Ce shop ne contient pas assez d'item.");
						    				} else if(DataAPI.getPlayer(event.getPlayer()).getMoney(false) < price) {
						    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous n'avez pas assez d'argent.");
						    				} else {
						    					GlobalFunction.transfertFromTo(chest.getBlockInventory(), event.getPlayer().getInventory(), is, amount);
						    					ShopManager.addMoneyToShop(event.getClickedBlock().getLocation(), price);
						    					DataAPI.getPlayer(event.getPlayer()).removeMoney(price, true, false);
						    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.GREEN + "Vous avez achet� " + amount + " " + LanguageHelper.getItemName(is, "fr_FR") + ".");
						    				}
							    		} else if(price_action.startsWith("V") && event.getAction() == Action.LEFT_CLICK_BLOCK) {
							    			if(GlobalFunction.getItemCount(event.getPlayer().getInventory(), is) < amount) {
							    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Minimum de "+amount+" pour vendre.");
							    			}
							    			else if(!GlobalFunction.canAddItem(chest.getBlockInventory(), is, amount)) {
							    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Le coffre du vendeur est plein.");
						    				} 
							    			else if(ShopManager.getShopMoney(sign.getLocation()) < price) {
							    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Le vendeur n'a pas assez d'argent.");
							    			} else {
						    					GlobalFunction.transfertFromTo(event.getPlayer().getInventory(), chest.getBlockInventory(), is, amount);
						    					ShopManager.removeMoneyFromShop(event.getClickedBlock().getLocation(), price);
						    					DataAPI.getPlayer(event.getPlayer()).addMoney(price, true, true);
						    					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.GREEN + "Vous avez vendu " + amount + " " + LanguageHelper.getItemName(is, "fr_FR") + ".");
						    				}
						    			}
					    			}
					    		}
			    			} else {
			    				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous n'avez pas la permission d'utiliser ce shop.");
			    			}
			    		}
			    	} catch(Exception ex) {
		    			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Ce panneau de vente a un probl�me. Contactez son propri�taire.");
		    			ex.printStackTrace();
		    		}
		    		event.setCancelled(true);
		    	}
		    }
		}
	}
}
