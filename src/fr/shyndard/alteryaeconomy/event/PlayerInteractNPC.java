package fr.shyndard.alteryaeconomy.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaeconomy.Main;
import fr.shyndard.alteryaquest.api.QuestAPI;
import net.citizensnpcs.api.event.NPCLeftClickEvent;

public class PlayerInteractNPC implements Listener {

	@EventHandler
	public void onInteract(NPCLeftClickEvent event) {
		if(event.isCancelled()) return;
		if(Main.getBankerIdList().get(event.getNPC().getId()) == null) return;
		PlayerInformation pi = DataAPI.getPlayer(event.getClicker());
		if(pi.hasPermission("banker.access")) {
			NPCAPI.talkTo(event.getNPC(), event.getClicker(), "Le temps, c'est de l'argent !");
			NPCAPI.answer(event.getClicker(), "Je veux consulter mon compte", "/bank consult " + event.getNPC().getId());
			NPCAPI.answer(event.getClicker(), "Je veux d�poser de l'argent", "/bank add " + event.getNPC().getId());
			NPCAPI.answer(event.getClicker(), "Je veux retirer de l'argent", "/bank remove " + event.getNPC().getId());
		} else QuestAPI.sendRandomMessage(event.getNPC(), event.getClicker());
		event.setCancelled(true);
	}
	
}