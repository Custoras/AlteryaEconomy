package fr.shyndard.alteryaeconomy.event;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.inventory.ItemStack;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaeconomy.Main;
import fr.shyndard.alteryaeconomy.manager.ShopManager;
import net.md_5.bungee.api.ChatColor;

public class PlayerCreateShop implements Listener {

	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		if(!event.getLine(0).equalsIgnoreCase("[Shop]")) return;
		if(!DataAPI.getPlayer(event.getPlayer()).hasPermission("economy.createshop")) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous n'avez pas la permission de cr�er un shop.");
			event.getBlock().breakNaturally();
			return;
		}
		if(!ShopManager.canCreateShop(event.getPlayer()) && !Main.getEditorList().contains(event.getPlayer())) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Vous ne pouvez pas cr�er plus de 15 shops.");
			event.getBlock().breakNaturally();
			return;
		}
		if(ShopManager.isShop(event.getBlock().getLocation())) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Il y a d�j� un shop ici. Non ? Alors appelez un membre du staff.");
			event.getBlock().breakNaturally();
		}
		String[] item_data = event.getLine(1).split(":");
		if(item_data.length == 0) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Ligne de d�finition de l'item vide (ligne 2)");
			event.getBlock().breakNaturally();
			return;
		}
		Integer amount = null;
		try {
			amount = Integer.parseInt(event.getLine(3));
		} catch(Exception ex) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Montant incorrect (ligne 4)");
			event.getBlock().breakNaturally();
			return;
		}
		try {
			ItemStack is = null;
			if(item_data.length == 2) {
				is = new ItemStack(Material.matchMaterial(item_data[0]), amount, Short.parseShort(item_data[1]));
			} 
			else if(item_data.length == 1) {
				is = new ItemStack(Material.matchMaterial(item_data[0]), amount);
			}
			event.setLine(1, is.getType().name() + (item_data.length == 2 ? ":" + item_data[1] : ""));
		} catch(Exception ex) {
			event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Mauvaise d�finition de l'item (ligne 2)");
			event.getBlock().breakNaturally();
			return;
		}
		
			String[] action_data = event.getLine(2).split(" ");
			if(action_data.length < 1 || action_data.length > 2) {
				event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Ligne de d�finition des actions vide (ligne 3)");
				event.getBlock().breakNaturally();
				return;
			}
			for(String action : action_data) {
				String[] action_data_args = action.split(":");
				if(action_data_args.length != 2 || (!action_data_args[0].equals("A") && !action_data_args[0].equals("V"))) {
					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Mauvaise d�claration d'une action (ligne 3)");
					event.getBlock().breakNaturally();
					return;
				}
				Float price = null;
				try {
					price = Float.parseFloat(action_data_args[1]);
				} catch(Exception ex) {
					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Prix d'une action invalide (ligne 3)");
					event.getBlock().breakNaturally();
					return;
				}
				if(price <= 0) {
					event.getPlayer().sendMessage(Main.getShopPrefix() + ChatColor.RED + "Le prix doit �tre sup�rieur � z�ro (ligne 3)");
					event.getBlock().breakNaturally();
					return;
				}
			}
			if(Main.getEditorList().contains(event.getPlayer())) {
				ShopManager.addAdminShop(event.getBlock().getLocation());
			} else {
				ShopManager.addPlayerShop(event.getBlock().getLocation(), event.getPlayer());
			}
			event.getPlayer().sendMessage(Main.getShopPrefix() + "Shop " + ChatColor.GREEN + "cr��" + ChatColor.GRAY+".");
		event.setLine(0, Main.getFirstSignLine());
	}
}
