package fr.shyndard.alteryaeconomy.event;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaeconomy.Main;

public class PlayerChat implements Listener {
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		if(Main.getPlayerCantSpeak().get(event.getPlayer()) == null) return;
		event.setCancelled(true);
		Float value = null;
		try {
			value = Float.parseFloat(event.getMessage());
		} catch(Exception ex) {
			event.getPlayer().sendMessage(Main.getBankPrefix() + ChatColor.RED + "Saisissez une valeur enti�re ou � virgule.");
		}
		if(value < 10) {
			event.getPlayer().sendMessage(Main.getBankPrefix() + ChatColor.RED + "Saisissez une valeur d'au moins 10.");
			return;
		}
		PlayerInformation pi = DataAPI.getPlayer(event.getPlayer());
		if(Main.getPlayerCantSpeak().get(event.getPlayer()) == 0) {
			if(pi.getMoney(false) >= value) {
				pi.removeMoney(value, false, false);
				pi.addMoney(value, false, true);
				event.getPlayer().sendMessage(Main.getBankPrefix() + "Vous avez d�pos� " + ChatColor.GOLD + value + DataAPI.getMoneySymbol() + ChatColor.GRAY + " en banque.");
			} else {
				event.getPlayer().sendMessage(Main.getBankPrefix() + ChatColor.RED + "Vous n'avez pas assez d'argent dans votre bourse.");
			}
		} else {
			if(pi.getMoney(true) >= value) {
				if(pi.getMoney(false) + value > pi.getMaxMoney()) {
					event.getPlayer().sendMessage(Main.getBankPrefix() + ChatColor.RED + "Il n'y a pas assez de place dans votre bourse.");
				} else {
					pi.removeMoney(value, false, true);
					pi.addMoney(value, false, false);
					event.getPlayer().sendMessage(Main.getBankPrefix() + "Vous avez retir� " + ChatColor.GOLD + value + DataAPI.getMoneySymbol() + ChatColor.GRAY + " de la banque.");
				}
			} else {
				event.getPlayer().sendMessage(Main.getBankPrefix() + ChatColor.RED + "Vous n'avez pas assez d'argent en banque.");
			}
		}
		Main.getPlayerCantSpeak().put(event.getPlayer(), null);
	}
}
