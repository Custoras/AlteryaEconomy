package fr.shyndard.alteryaeconomy;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class CmdBank implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("bank.access")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length != 2) {
			sender.sendMessage(Main.getBankPrefix() + ChatColor.RED + "Votre banquiez semble dormir. Ne faites pas de bruit.");
			return true;
		}
		Integer npc_id = null;
		try {
			npc_id = Integer.parseInt(args[1]);
		} catch(Exception ex) {
			sender.sendMessage(Main.getBankPrefix() + ChatColor.RED + "Voulez vous parlez � votre banquier imaginaire ?");
			return true;
		}
		if(Main.getBankerIdList().get(npc_id) == null) {
			sender.sendMessage(Main.getBankPrefix() + ChatColor.RED + "Cet habitant n'est pas un banquier...");
			return true;
		}
		NPC npc = CitizensAPI.getNPCRegistry().getById(npc_id);
		if(player.getLocation().distance(npc.getStoredLocation()) > 5) {
			sender.sendMessage(Main.getBankPrefix() + ChatColor.RED + "Vous �tes trop �loign� pour parler.");
			return true;
		}
		if(args[0].equals("consult")) {
			NPCAPI.talkTo(npc, player, "Vous avez " + ChatColor.GOLD + pi.getMoney(true) + DataAPI.getMoneySymbol() + ChatColor.GRAY + " en banque.");
		}
		else if(args[0].equals("add")) {
			NPCAPI.talkTo(npc, player, "Indiquez le montant � d�poser sur votre compte.");
			Main.getPlayerCantSpeak().put(player, 0);
		}
		else if(args[0].equals("remove")) {
			NPCAPI.talkTo(npc, player, "Indiquez le montant � retirer de votre compte.");
			Main.getPlayerCantSpeak().put(player, 1);
		}
		return true;
	}
}
