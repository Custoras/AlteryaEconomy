package fr.shyndard.alteryaeconomy.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaeconomy.Main;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class GlobalFunction {

	public static void loadBanker() {
		Main.getBankerIdList().clear();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT banker_id FROM banker_list");
			while(result.next()) {
				createBankerSign(result.getInt("banker_id"));
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	static void createBankerSign(Integer npc_id) {
		NPC npc = CitizensAPI.getNPCRegistry().getById(npc_id);
		if(npc == null) return;
		npc.getStoredLocation().getChunk().load();
		for(Entity e : npc.getStoredLocation().getWorld().getNearbyEntities(npc.getStoredLocation(), 2, 2, 2)) {
			if(e.getType() == EntityType.ARMOR_STAND && e.getCustomName() != null) {
				if(((ArmorStand)e).getCustomName().equals("" + ChatColor.GOLD + ChatColor.BOLD + DataAPI.getMoneySymbol())) e.remove();
			}
		}
		ArmorStand as = (ArmorStand)Bukkit.getWorlds().get(0).spawnEntity(npc.getStoredLocation().clone().add(0,0.1,0), EntityType.ARMOR_STAND);
		as.setBasePlate(false);
		as.setGravity(false);
		as.setVisible(false);
		as.setCustomNameVisible(true);
		as.setCustomName("" + ChatColor.GOLD + ChatColor.BOLD + DataAPI.getMoneySymbol());
		Main.getBankerIdList().put(npc_id, as);
	}
	
	@SuppressWarnings("deprecation")
	public static int getItemCount(Inventory inv, ItemStack item) {
		int count = 0;
		for(ItemStack is : inv.getContents()) {
			if(is != null && is.getType() == item.getType() && is.getData().getData() == item.getData().getData() && is.getDurability() == item.getDurability()) {
				count += is.getAmount();
			}
		}
		return count;
	}

	@SuppressWarnings("deprecation")
	public static void removeItem(Player player, ItemStack item, int count) {
		int remove = 0;
		for(ItemStack is : player.getInventory().getContents()) {
			if(remove>= count) break;
			if(is != null && is.getType() != Material.AIR) {
				if(is.getType() == item.getType() && is.getData().getData() == item.getData().getData()) {
					if(remove + is.getAmount() > count) {
						is.setAmount(is.getAmount()-(count-remove));
						break;
					} else {
						remove+=is.getAmount();
						is.setAmount(0);
					}
				}
			}
		}
		player.updateInventory();
	}

	@SuppressWarnings("deprecation")
	public static void transfertFromTo(Inventory from, Inventory to, ItemStack ref, int count) {
		int remove = 0;
		for(ItemStack is : from.getContents()) {
			if(remove>= count) break;
			if(is != null && is.getType() == ref.getType() && is.getData().getData() == ref.getData().getData() && is.getDurability() == ref.getDurability()) {
				if(remove + is.getAmount() > count) {
					is.setAmount(is.getAmount()-(count-remove));
					ItemStack toAdd = is.clone();
					toAdd.setAmount(count-remove);
					to.addItem(toAdd);
					break;
				} else {
					ItemStack toAdd = is.clone();
					toAdd.setAmount(is.getAmount());
					to.addItem(toAdd);
					remove+=is.getAmount();
					is.setAmount(0);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static boolean canAddItem(Inventory from, ItemStack ref, int require) {
		for(ItemStack is : from.getContents()) {
			if(is == null || is.getType() == Material.AIR) return true;
			else if(is.getType() == ref.getType() && is.getData().getData() == ref.getData().getData() && is.getDurability() == ref.getDurability()) {
				if(is.getAmount()+require <= 64) return true;
			}
		}
		return false;
	}
}
