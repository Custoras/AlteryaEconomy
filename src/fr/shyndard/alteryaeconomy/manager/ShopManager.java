package fr.shyndard.alteryaeconomy.manager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;

public class ShopManager {

	public static boolean canCreateShop(Player player) {
		try {
			String sql = "SELECT count(owner_id) AS count FROM shop WHERE owner_id = ?";
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, DataAPI.getPlayer(player).getId(), Types.INTEGER);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				return rs.getInt("count") < 15;
			}
		} catch(Exception ex) { ex.printStackTrace(); }
		return false;
	}
	
	public static boolean isAdminShop(Location loc) {
		try {
			String sql = "SELECT 1 FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ? AND owner_id = 0";
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) return true;
		} catch(Exception ex) { ex.printStackTrace(); }
		return false;
	}

	public static boolean isPlayerShop(Location loc) {
		try {
			String sql = "SELECT 1 FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ? AND owner_id > 0";
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) return true;
		} catch(Exception ex) { ex.printStackTrace(); }
		return false;
	}
	
	public static boolean isShop(Location loc) {
		try {
			String sql = "SELECT 1 FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ?";
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) return true;
		} catch(Exception ex) { ex.printStackTrace(); }
		return false;
	}
	
	public static void addAdminShop(Location loc) {
		try {
			String sql = "INSERT INTO shop VALUES (NULL, ?, ?, ?, ?, 0)"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void addPlayerShop(Location loc, Player player) {
		try {
			String sql = "INSERT INTO shop VALUES (NULL, ?, ?, ?, ?, ?)"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			statement.setObject(5, DataAPI.getPlayer(player).getId(), Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void removeShop(Location loc) {
		try {
			String sql = "DELETE FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ?"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void addMoneyToShop(Location loc, float amount) {
		try {
			String sql = "UPDATE player_information SET bank = bank + ? WHERE id = (SELECT owner_id FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ?)"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, amount, Types.FLOAT);
			statement.setObject(2, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(3, loc.getBlockX(), Types.INTEGER);
			statement.setObject(4, loc.getBlockY(), Types.INTEGER);
			statement.setObject(5, loc.getBlockZ(), Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	public static float getShopMoney(Location loc) {
		try {
			String sql = "SELECT bank FROM player_information AS PI JOIN shop ON shop.owner_id = PI.id WHERE world = ? AND x = ? AND y = ? AND z = ?";
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(2, loc.getBlockX(), Types.INTEGER);
			statement.setObject(3, loc.getBlockY(), Types.INTEGER);
			statement.setObject(4, loc.getBlockZ(), Types.INTEGER);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) return rs.getFloat("bank");
		} catch(Exception ex) { ex.printStackTrace(); }
		return 0;
	}
	
	public static void removeMoneyFromShop(Location loc, Float price) {
		try {
			String sql = "UPDATE player_information SET bank = bank - ? WHERE id = (SELECT owner_id FROM shop WHERE world = ? AND x = ? AND y = ? AND z = ?)"; 
			PreparedStatement statement = DataAPI.getSql().getConnection().prepareStatement(sql);
			statement.setObject(1, price, Types.FLOAT);
			statement.setObject(2, loc.getWorld().getName(), Types.VARCHAR);
			statement.setObject(3, loc.getBlockX(), Types.INTEGER);
			statement.setObject(4, loc.getBlockY(), Types.INTEGER);
			statement.setObject(5, loc.getBlockZ(), Types.INTEGER);
			statement.executeUpdate(); 
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}	
}
