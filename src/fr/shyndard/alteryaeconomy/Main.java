package fr.shyndard.alteryaeconomy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.alteryaeconomy.event.PlayerChat;
import fr.shyndard.alteryaeconomy.event.PlayerCreateShop;
import fr.shyndard.alteryaeconomy.event.PlayerInteractNPC;
import fr.shyndard.alteryaeconomy.event.PlayerInteractShop;
import fr.shyndard.alteryaeconomy.function.GlobalFunction;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {

	public static boolean debug = false;
	private static List<Player> editor_list = new ArrayList<>();
	private static List<Player> armorstand_list = new ArrayList<>();
	private static Map<Integer, ArmorStand> banker_id_list = new HashMap<>();
	private static Map<Player, Integer> player_cant_speak = new HashMap<>();
	public static List<Player> add_admin_shop = new ArrayList<>();
	
	public void onEnable() {
		getCommand("bank").setExecutor(new CmdBank());
		getCommand("shop").setExecutor(new CmdShop());
		
		getServer().getPluginManager().registerEvents(new PlayerInteractShop(), this);
		getServer().getPluginManager().registerEvents(new PlayerCreateShop(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractNPC(), this);
		getServer().getPluginManager().registerEvents(new PlayerChat(), this);
		
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
	          public void run() {
	        	  GlobalFunction.loadBanker();
	          }    
	  	}, 5*20L);
	}
	
	public void onDisable() {
		for(ArmorStand as : PlayerInteractShop.armorstand_location.values()) {
			as.getLocation().getChunk().load();
			as.remove();
		}
		for(ArmorStand as : getBankerIdList().values()) {
			as.getLocation().getChunk().load();
			as.remove();
		}
	}
	
	public static Map<Player, Integer> getPlayerCantSpeak() {
		return player_cant_speak;
	}
	
	public static String getFirstSignLine() {
		return "[" + ChatColor.AQUA + "Shop" + ChatColor.BLACK + "]";
	}
	
	public static String getShopPrefix() {
		return "[" + ChatColor.AQUA + "Shop" + ChatColor.WHITE + "] " + ChatColor.GRAY;
	}
	
	public static String getBankPrefix() {
		return "[" + ChatColor.GOLD + "Banque" + ChatColor.WHITE + "] " + ChatColor.GRAY;
	}
	
	public static List<Player> getEditorList() {
		return editor_list;
	}
	
	public static List<Player> getArmorStandList() {
		return armorstand_list;
	}
	
	public static Map<Integer, ArmorStand> getBankerIdList() {
		return banker_id_list;
	}
}
