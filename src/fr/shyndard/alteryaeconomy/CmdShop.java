package fr.shyndard.alteryaeconomy;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import net.md_5.bungee.api.ChatColor;

public class CmdShop implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("shop.admin")) {
			sender.sendMessage(ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length >= 1) {
			if(args[0].equalsIgnoreCase("debug")) {
				if(Main.debug) Main.debug = false;
				else Main.debug = true;
				sender.sendMessage(ChatColor.GREEN + "Debug " + Main.debug);
			}
			else if(args[0].equalsIgnoreCase("add")) {
				if(Main.add_admin_shop.contains(player)) {
					Main.add_admin_shop.remove(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.RED + "quittez" + ChatColor.GRAY + " le mode ajout.");
				} else {
					Main.add_admin_shop.add(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.GREEN + "entrez" + ChatColor.GRAY + " dans le mode ajout.");
				}
			}
			else if(args[0].equalsIgnoreCase("editor")) {
				if(Main.getEditorList().contains(player)) {
					Main.getEditorList().remove(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.RED + "quittez" + ChatColor.GRAY + " le mode �dition.");
				} else {
					Main.getEditorList().add(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.GREEN + "entrez" + ChatColor.GRAY + " dans le mode �dition.");
				}
			}
			else if(args[0].equalsIgnoreCase("armorstand")) {
				if(Main.getArmorStandList().contains(player)) {
					Main.getArmorStandList().remove(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.RED + "quittez" + ChatColor.GRAY + " le mode armorstand.");
				} else {
					Main.getArmorStandList().add(player);
					sender.sendMessage(Main.getShopPrefix() + "Vous " + ChatColor.GREEN + "entrez" + ChatColor.GRAY + " dans le mode armorstand.");
				}
			} else {
				sender.sendMessage(Main.getShopPrefix() + "/shop editor");
				sender.sendMessage(Main.getShopPrefix() + "/shop armorstand");
			}
		} else {
			sender.sendMessage(Main.getShopPrefix() + "/shop editor");
			sender.sendMessage(Main.getShopPrefix() + "/shop armorstand");
		}
		return true;
	}

}
